<div id="top"></div>
<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/sonibble-creators/products/kits-templates/email/contact-email-template">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/39870121/email.png" alt="Logo" width="100" height="100">
  </a>

  <h1 align="center">Contact Email Template</h1>

  <p align="center">
      Fully feature contact email template
    <br />
    <a href="https://gitlab.com/sonibble-creators/products/kits-templates/email/contact-email-template/-/wikis"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/sonibble-creators/products/kits-templates/email/contact-email-template/">View Demo</a>
    ·
    <a href="https://gitlab.com/sonibble-creators/products/kits-templates/email/contact-email-template/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/sonibble-creators/products/kits-templates/email/contact-email-template/-/issues">Request Feature</a>
  </p>
</div>

<br />
<br />

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li><a href="#about-the-project">About The Project</a></li>
    <li><a href="#getting-started">Get Started</a></li>
    <li><a href="#wiki">Wiki</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

<br />

<!-- ABOUT THE PROJECT -->
<!-- all about the project, specify the background -->

## About The Project

This project is purposed to provide free and open email template for user that want to build email service to send some email, contact and even the programatically send a contact email from website using some provider like sendinblue, mailchimp, mailjs, etc.

You can easyly just copy and paste the current email templates

<br/>
<p align="right">(<a href="#top"><b>back to top</b></a>)</p>

## Gettting Started

This project use the node module to install some dependencies like tailwind to work. So please ensure you conplete all requirement stacks

### Requirements

- NodeJS
- TailwindCss
- Live Server Extension (VSCODE)

   <br/>
 <br/>

### Instalation

1. First install and run the tailwind in local

```bash

# install all dependencies and download it
npm install --legacy-peer-deps

# run the tailwind to watch the content
# and generate the main.css file
npm run dev

```

2.  Run the Live Server in (VS CODE)
3.  Now you're ready to develop

 <br/>
 <br/>

### How To Use

1. To use the template you just need to gong into the file like index.html, or reply.html in pages

2. Copy the content and bring it into the mail service. Then change the css file content using the cloud version using this Raw gitlab file

3. Now you're ready

<!-- Wiki -->
<!-- enable the user to see the wiki of this project -->

## Wiki

We build this project with some record of our documentation, If you interest to see the all about this project please check the wiki.

_For more detail, please refer to the [Wiki](https://gitlab.com/sonibble-creators/products/kits-templates/email/contact-email-template/-/wikis)_

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>

<!-- ROADMAP -->
<!-- Initial info roadmap of this project -->

## Roadmap

- [x] Add basic feature for contact
- [x] Make if become fully responsive
- [x] Complete and easy setup

See the [open issues](https://gitlab.com/sonibble-creators/products/kits-templates/email/contact-email-template/-/issues) for a full list of proposed features (and known issues).

Refere to changelog to see the detail [CHANGELOG](CHANGELOG.md)

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".

Please check the contributing procedure [here](CONTRIBUTING.md), Don't forget to give the project a star! Thanks again!

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>

<!-- LICENSE -->

## License

Distributed under the MIT License. See [LICENSE](LICENSE.md) for more information.

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>

<!-- CONTACT -->

## Contact

Nyoman Sunima - [@nyomansunima](https://instagram.com/nyomansunima) - nyomansunima@gmail.com

Sonibble - [@sonibble](https://instagram.com/sonibble) - [creative.sonibble@gmail.com](mailto:creative.sonibble@gmail.com) - [@sonibble](https://twitter.com/sonibble)

Project Link: [Contact Email Template](https://gitlab.com/sonibble-creators/products/kits-templates/email/contact-email-template)

<p align="right">(<a href="#top"><b>back to top</b></a>)</p>
